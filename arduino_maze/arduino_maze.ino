#include <SoftwareSerial.h>

// TX/RX
SoftwareSerial Bluetooth(11, 10);

void setup() {
  Serial.begin(38400);
  Serial.println("Serial Started");
  Bluetooth.begin(38400);
  Serial.println("Send 's' to sniff packets, 'h' to halt packets:");
  delay(2000);
  
}

unsigned long pack = 0;

void loop() {
  // Read from HC-05 and send data to the Arduino Serial Monitor
  if (Bluetooth.available()) {
    Serial.write(Bluetooth.read());
  }

  // Read from Arduino Serial Monitor and send it to the HC-05
  if (Serial.available()) {
    char com = Serial.read();
    Serial.write(com);
    if (com == 's') {
      Serial.println();
      PrintBits32(pack);
    }
    Bluetooth.write(com);
  }
}

void PrintBits32(unsigned long u) {
  char str[34];
  str[32] = '\n';
  str[33] = 0;
  for (int i = 31; i >= 0; i--)
  {
    str[i] = '0' + (u & 1);
    u >>= 1;
  }

  Serial.print(str);
}
